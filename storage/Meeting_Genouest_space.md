### Meeting to discuss about Genouest disk space issues

* servers available:

|Server|Total storage|Available|storage type|
|----|----|----|----|
|Genouest|72 T|**870 G !**|hot data|
|Nanoserver|100 T|75 T|cold data|
|MSA|200 T|200 T|cold data|
|CaniIGDR|40T/80cpus|40 T|hot data|

* Note from Louis: It exists Singularity images for many tools. They are are available here (e.g minimap) :
	- `/cvmfs/singularity.galaxyproject.org/m/i/minimap2:2.21--h5bf99c6_0`
	- [x] ToDo :  Make a common folders in : `/groups/dog/script/singularity/all/`

* Increase Genouest futur space
	* [X]  email  Anthony Bretaudeau to increase futur disk space via ≠ fundings : see [Notes meetings 12/7/2024](https://gitlab.com/bioinfog/nanopore/general/igdrion_pgd/-/blob/main/CR.md)
		* [x] IGDRion : 10k€ done
		* [X] ACGO = XXX? asked Catherine (depends FRM)
		* [X] MSD =  20k€ : see Note above
		* [X] Paleogenomic => depends on ANR results... not funded :-( 
* Process to clean data
	* check the rigths -> ~tderrien/bin/giveRight.sh
	* **Copy/rclone FASTQ and then remove from Genouest** 
	* to rclone data, feel free to see : https://gitlab.com/bioinfog/general-informations/-/blob/master/storage/rclone_msa.md?ref_type=heads
* List folder to be checked/copied
	- `canFam3` : :warning: **to be copied on msaa4**
		- [ ] Stephanie/Benoit `/groups/dog/data/canFam3/Melanoma`: keep only `.bam/.bai` ?
		- [ ] Stephanie/Benoit : `/groups/dog/data/canFam3/NGS/Hystiocytic_sarcoma/DNA/WGS/` => remove?
		- [ ] Stephanie/Benoit : `/groups/dog/data/canFam3/NGS/BGI-temp/F18FTSEUHT1416_DOGpwqR/RESULTS/CNV_WES/` (503Go)
		- [X] Thomas : `/groups/dog/bam_analysis/` ?
		- [X] Thomas : Copy msa old HOME solenne, ckergal, mlorthiois
			- On nanoserver `/Volumes/home/` 
		- [X] Thomas `/groups/dog/data/canFam3/NGS/Longevity/`
			- => keep while waiting for imputation finsished on canFam4
		- [ ] Pascale : `/groups/dog/data/canFam3/NGS/PRA/`
		- [ ] Pascale : `/groups/dog/data/canFam3/NGS/Epilepsy/`
		- [X] Thomas : `/groups/dog/data/canFam3/NGS/Dog_Reannotation/RNA/BAM/` copy/rclone 
			- deleted the `.bam` and kept the fastq.
		- [X] Thomas :  `/groups/dog/data/canFam3/NGS/Glioma/RNA/`
			-  copied in `storage_dsi:rv436-msaa4/canFam3/NGS/Glioma/RNA/`
		- [X] Thomas : `/groups/dog/data/canFam3/NGS/Testes_RNASeq/RNA/` => Joce  data
			- copied here `storage_dsi:rv436-msaa4/canFam3/NGS/Testes_RNASeq/RNA/`
	- `canFam4` : :warning: **to be copied on msaa4**
		- [ ] Pascale   : `/groups/dog/data/canFam4/NGS/neuropathy/` 
		- [ ] Stéphanie/Benoit : `/groups/dog/data/canFam4/NGS/Hystiocytic_sarcoma/` 
		- [ ] Christophe : `/groups/dog/data/canFam4/NGS/Ancient/` 
		- [ ] Christophe : `/groups/dog/data/canFam4/NGS/cardiac_nantes/` 
	- [ ] `IGDRion :`  :warning: **copied on msab3**
		- [X] Thomas/Aurore :  rm `.fast5 , bam , fastq`
		- [X] Thomas/Aurore :  keep `.txt` files

For information, here are the disk space per important folder (30 June 2024):
```
=> personal folders
...
896G /groups/dog/script/
961G /groups/dog/stage/
974G /groups/dog/tderrien/
1.1T /groups/dog/anastasia/
1.5T /groups/dog/aurore/
2.0T /groups/dog/bhedan/
4.0T /groups/dog/smottier/
12T /groups/dog/hitte/
14T /groups/dog/nanopore/
33T /groups/dog/data/

=> /groups/dog/data/canFam3/NGS/

8.0K /groups/dog/data/canFam3/NGS/Melanoma_ocular/
6.5M /groups/dog/data/canFam3/NGS/Osteo_sarcoma/
8.4G /groups/dog/data/canFam3/NGS/MIR/
20G /groups/dog/data/canFam3/NGS/Antagene/
42G /groups/dog/data/canFam3/NGS/Ancient/
185G /groups/dog/data/canFam3/NGS/DoGA_CAGE-STRT/
195G /groups/dog/data/canFam3/NGS/Digit_Dev/
199G /groups/dog/data/canFam3/NGS/Testes_RNASeq/
302G /groups/dog/data/canFam3/NGS/dog10k/
364G /groups/dog/data/canFam3/NGS/Glioma/
603G /groups/dog/data/canFam3/NGS/Epilepsy/
925G /groups/dog/data/canFam3/NGS/Dog_Reannotation/
1.4T /groups/dog/data/canFam3/NGS/BGI-temp/
1.5T /groups/dog/data/canFam3/NGS/PRA/
2.0T /groups/dog/data/canFam3/NGS/Dysplasia/
4.4T /groups/dog/data/canFam3/NGS/Longevity/
4.7T /groups/dog/data/canFam3/NGS/Hystiocytic_sarcoma/
5.1T /groups/dog/data/canFam3/NGS/Melanoma/

=> /groups/dog/data/canFam4/NGS/

138G /groups/dog/data/canFam4/NGS/CAGE_DoGA/
146G /groups/dog/data/canFam4/NGS/PRA/
541G /groups/dog/data/canFam4/NGS/epilepsy/
827G /groups/dog/data/canFam4/NGS/Longevity/
854G /groups/dog/data/canFam4/NGS/neuropathy/
1.1T /groups/dog/data/canFam4/NGS/Hystiocytic_sarcoma/
1.3T /groups/dog/data/canFam4/NGS/Ancient/
1.9T /groups/dog/data/canFam4/NGS/cardiac_nantes/

```