#!/bin/bash

# input variable
PROGNAME=$(basename $0)
DIR=$1

# create usage function that will be called each time there is an ERROR
# >&2 	: print to STDERR
usage() {
    echo "#" >&2
    echo -e "# USAGE: $PROGNAME <DIRECTORY>" >&2
    echo -e "# EXAMPLE :  $PROGNAME project_dir" >&2
    exit 1;
}

# Test number of parameters
###########################
if [[ $# -lt 1 ]]; then
	echo "ERROR : wrong number of arguments">&2
	# call usage function
	usage;
fi

##############################

# rclone module
. /local/env/envconda.sh
conda activate ~tderrien/env/rclone/

# Define source and destination
SOURCE="/groups/dog/${DIR}"
DEST="storage_dsi:/rv436-msaa4/${DIR}"

# create directory and copy files
rclone mkdir $DEST
rclone copy --bwlimit 10M $SOURCE $DEST -v --checksum --human-readable

# to launch script: 
## sbatch rclone_copy_Genouest2MSA.sh <DIRECTORY_TO_COPY>

# WARNINGS :
## if multiple copies using rclone in the same time, avoid exceeding 30 Mb/sec in total !
