# FAIR training - october 2024

## Organisation

Les plateformes de bioinformatique du réseau Biogenouest: ABiMS, BiRD, GenOuest et SeBiMER.

**Equipe pédagogique**
- Anthony Bretaudeau @abretaud - GenOuest - CNRS (Rennes)
- Audrey Bihouée @abihouee - BiRD - Univ Nantes (Nantes)
- Eric Charpentier @echarpentier - BiRD - Univ Nantes (Nantes)
- Alexandre Cormier @cormalex - IFREMER (Brest)
- Erwan Corre @r1corre - Station Biologique de Roscoff - CNRS / Sorbonne Université (Roscoff)
- Patrick Durand @pgdurand - IFREMER (Brest)
- Gildas Le Corguillé @lecorguille - Station Biologique de Roscoff - Sorbonne Université / CNRS (Roscoff)
- Stéphanie Robin @stephanirobin - GenOuest/BIPAA - INRAE (Rennes)
- Raluca Teusan @rteusan - BiRD - Univ Nantes (Nantes)

**Objectifs pédagogiques**
Lors de cette formation, nous vous présenterons les principes “FAIR” (Facile à trouver, Accessible, Interopérable, Réutilisable) et leur application dans les projets d’analyse et de développement.
Des présentations théoriques suivies d’utilisations pratiques de plusieurs outils permettant d’améliorer la reproductibilité des analyses seront proposées.

## Programme

- Intro FAIR
- Organiser son projet
- Présentation des infrastructures / SLURM
- Les Notebooks + TP jupyter notebook
- Initiation à Conda + TP
- Initiation aux Containers + TP
- Introduction à Git + TP
- Workflows + TP (Galaxy, NF, SMK)

## Résumé

### 1- Principes FAIR

Voir [pdf FAIR principles](fairbioinfo-go-2024_introduction-fair_slides.pdf)

- FAIRChecker: interface web pour évaluer métriques FAIR sur des données
- DMP: pour formaliser le cycle de vie de la donnée + réfléchir entre collègues (exploitation, propriété et futur des data)
- importance de travailler en amont bioinfo+bio pour déterminer le plan d'expérience, les données à générer (par qui, quand, comment ?), la taille des données générées, leur utilité pour d'autres, les ressources nécessaires pour les analyses (vient souvent trop tard dans la discussion/projet)
- ressources nécessaires à réfléchir en amont: gestion, stockage, cout
- sécurisation des données -> privilégier entrepots européens comme ENA (European Nucleotide Archive)
    - plus stricte niveau metadata à fournir (+ chiant à faire mais meilleure info metadata que sur NCBI par ex)
    - pipelines de vérification des metadata pour ENA pour faciliter le travail de mise en place: gitlab/ifremer/athena
    - stockage à différents endroits: 
        - data center: raw data + metadata 
        - publication ENA/SRA
        - zenodo (dépot de tables de comptages avec info pour trouver metadata/raw data)

-> voir diapos 44 à 49 pour DMP + questions à se poser pour les données d'un projet

### 2- Organiser son projet

Voir [pdf Organiser son projet](fairbioinfo-go-2024_project-organisation_slides.pdf)

projet = collaboration la plupart du temps donc nécessaire de mettre des règles en place dès le début
Comment faire pour bien organiser son projet ?
- nom de projet court et significatif
- organisation des repertoires + README + règles de nommages et organisation sous-repertoires
- infrastructure à réfléchir si backup mise en place sur certains types de fichiers
- raw data in separate secure, read only directory !!!
- README à la racine avec infos nécessaires au projet
    - outil pour créer template: [readmi](https://readmi.xyz/)
- règle de nommage des fichiers
    - date format ISO8601
    - peu de metadata dans nommage des fichiers -> fichier spécifique metadata à part
    - utiliser les standards existants
    - utiliser des format de fichiers "open" -> diapo 21
- bonnes pratiques sur les codes/pipelines/software utilisés
    - dossier spécifique avec versions
    - avoir un jeu test fonctionnel pour vérifier que le pipeline fonctionne toujours malgré update de versions
- stockage et sécurité à déterminer ++
    - au moins 3 copies (2 sur différents supports + 1 off-site)
    - !! une donnée doit être essentielle avant d'être backupée car très couteux !!
    - raw data + scripts permettant de refaire l'analyse
- versioning: important !! (git)
- licence: important !!
    - ex CC-BY pour mettre sur ENA
    - besoin d'une licence pour mettre sur conda
    - vérifier s'il y a des règles propres à la tutelle pour les licences
    - choix licence:[lien1](https://chooser-beta.creativecommons.org/), [lien2](https://choosealicense.com/)


![Data management tips](data_management_tips.png)

### 3- Infrastructure/slurm

Voir [Infra slurm](fairbioinfo-go-2024_hpc-slurm_slides.pdf)

IFB services:
- outils dispo
- libraries R
- module apptainer (ex-singularity)
- administration de usegalaxy.fr
- déploiement d'outils: infra qui partagent le même set d'outil (pas le cas de genouest encore)
- Open OnDemand = web portal for ocmputing resources -> pratique car applications intéractives (jupyterlab,  rstudio avec monitoring slurm jobs) mais pas encore dispo sur genouest

Tips pour slurm:
- rajouter `--time`pour gagner en priorité si petit code à faire tourner nécessitant peu de temps (par défaut 14 jours max sur genouest et 1 mois sur Abims) -> permet d'etre envoyé dans les trous
- safe report pour avoir efficiency sur les cpus/memoire/temps par job id

Futur des plateformes:
- fusion ABIMS + Genouest avec arrivée Eskemm
- fusion Bird dans GLYCID = mésocentre avec services mutualisés (comprend les pays de la loire) + construction d'un data center pour 2026

### 4- Notebooks

Voir [pdf notebooks](fairbioinfo-go-2024_notebooks_slides.pdf)

Principaux notebooks:
- RMarkdown
    - accessible via Rstudio
    - possible mix de langages
    - création rapport html
- jupyter notebook
    - julia, python, r, bash compatible
    - possible de mixer un langage avec du bash dans cellules en rajoutant `#=bash`
- quarto
    - command line interface
    - support diapo formation fait en quarto -> se sauvegarde facilement

Accès 'On demand' sur ABIMS: [On demand](https://ondemand.sb-roscoff.fr/dex/auth/ldap/login?back=&state=kmnrm6scup3mfioexomieomyl)

### 5- Packaging

Voir [pdf packaging](fairbioinfo-go-2024_tool-packaging_slides.pdf)

Infos nécessaires pour être reproductibles pour l'analyse de données:

![outil](reproductibility.png)

Si reproductible:
- + simple à réutiliser
- + simple pour tracer bugs
- + simple à updater/adapter

Pour améliorer repro dans analyses:
- pipeline workflow/snakemake/galaxy (info provenance metadata)
- tools/workflow catalogs: bio.tools, nf-core
- gestion packages: CRAN, bioconductor, pip, homebrew
- gestion env: virtualEnv, conda
- container: apptainer/singularity, docker

**Conda:**
- dépot sur anaconda via channels
    - bioconda: outils bioinfo
    - conda forge: général
    - ne pas utiliser channel anaconda default (packages peuvent être payants !!)
    - éviter le channel r
- good practices:
    - un environnement par outil pour éviter pb versions dépendances
- to build conda package = recette
    - meta.yml:
        - test intégrité de l'outil
        - requirements (librairies nécesaires pour compiler l'outil + celles pour utiliser l'outil)
        - test pour vérifier que outil bien téléchargé et fonctionne
        - metadata (about): licence, summary, le lien pour trouver l'outil
    - build.sh = fichier d'installation en bash
        - peut être généré automatiquement par conda skeleton
- méthode rapide: conda build mais pas isolé de la base donc vérifier qu'il fonctionne sur une autre base !
- à publier avec pull request sur bioconda et conda-forge
    - manual curating donc peut être long
    - relancer si besoin
    - possible de participer en tant que reviewer de packages conda
- bien de dire que c'est installé avec conda dans publi -> meilleur que installation manuelle
- attention complication si utilisation d'un vieil environnement conda avec conflits de dépendances !!
- mamba résout mieux les arbres de dépendances que conda -> codé en C, + rapide

**Container**

Container/image = capacité d'executer un systeme d'exploitation sur un autre esystème d'exploitation 
image = stocké sur disque, peutê^tre partagé, contient la recette avec système/librairies 
container = quand on lance l'image et utilise les outils 

- virtual machine
    - permet d'isoler chaque logiciel sur une meme infra
    - isolé (avantage) mais lourd (inconvénient)
- container
    - moins lourd car utilise le système d'exploitation de l'hote
    - isolé du système et du réseau => sécurité ++

**Docker**
- 1er container
- inconvénient = besoin d'etre root
- avantage = bcp de repositories dispo
    - officiel Docker Hub !! peut être payant !!
    - alternative: [quay.io](https://quay.io/biocontainers) ou sur github/lab
- diapo 36-39 pour usage docker
- recipe = créer une image docker revient à créer un sytème linux en entier avec outils dont on a besoin
    - docker file = docker image recipe => la partie difficile à mettre en oeuvre (diapo 40)
    - creation de l'image binaire (diapo 41)

**Apptainer**

Financé par fondation linux pour garder une partie singularity en open access (singularity commence à mettre sous licence)
- même principe que Docker mais image immuable
- image avec home monté par defaut (différent de docker)
- permet de lancer juste script/code (docker permet de lancer un service)
- possible de convertir image docker en singularity
```
apptainer pull docker <path>
```
- même image file à créer entre apptainer et docker, syntaxe qui change (diapo 46)

**BioContainers**
- communautaire => distribution de packages et containers + aide à la création
- si package conda, création d'image docker/apptainer automatiquement
- possible de créer une nouvelle image basée sur image existante si outil manquant
- images sur [biocontainers](https://quay.io/organization/biocontainers)
- plus reproductible/standard que conda car figé et isolé du système de base et les versions ne changent pas => traçabilité ++
- attention: etre vigilant sur images choisies = bien documentées, environnement bien installé
    - catalogue d'images directement dispo sur genouest

### 6- git

Voir [pdf git](fairbioinfo-go-2024_git_slides.pdf)

- commandes usuelles -> voir diapo (recap diapo 23)
- `.git` contient tout l'historique
- quand utilisation de `add`, modifications sont ajoutées dans l'`index` (stage)
- être clair dans les messages commit (commits peuvent être lus par tout le monde et permet de rechercher un commit particulier)
- faire plusieurs petits commits au lieu qu'un seul gros au bout de 6 mois -> pas de suivi pendant 6 mois des modifications, risque de perte d'info (1 commit = réponse à un problème => plus facile a gérer si reviewers)

**Branching**
- permet de compartimenter pour tester d'autres fonctionnalités/implémentations
- diapo 26-29
```
# créer une nouvelle branche
git checkout -b <new_branch>
# pour savoir la branche active
git branch
# pour changer de branche
git checkout <branch_name>
```
- possible de rajouter 2 lignes dans bash profile pour avoir info de la branche active directement dans le prompt !

**Collaboration**
- how to start a new project on github/lab -> diapo 31-35
- utiliser une licence (free and open): [link1](https://ufal.github.io/public-license-selector/) ou [link2](https://choosealicense.com/)
- si on veut cloner sans faire demodif => clone via https
- si on veut participer => clone via ssh pour être reconnu
- par defaut, la branche d'ou on clone = `origin`
- ne pas faire `pull` directement, passer d'abord par `fetch` 
    - pull = fetch + merge
    - fetch en 1er permet de voir les différences avant de merger
    - permet de faire une copie à côté pour ne pas écraser
- déterminer au début du projet les roles des collaborateurs: 
    - owner
    - maintainer => beaucoup de droits comme `merge branch`
    - developer => peuvent proposer de nouvelles fonctionnalités
    - guest
    - rôles/droits peuvent être paramétrés
    - possible de donner des tâches de vérification de code en nommant une personne

**Conflits**
- conflits si modifications dans une zone proche entre 2 commits différents
- marker de conflit <<<<< ou >>>>>
- git est très verbeux, il va demander de régler le conflit et donner une version de commit
- refaire modif (merge des conflits) dans un nouveau fichier puis `git add` et `git commit`

Important de bien communiquer avant de commencer quelque chose => possible de faire des `merge request`pour dire ce qu'on va faire en amont pour éviter conflits

![git workflow](git_workflow.png)

**Tips/Tricks**
- README à la racine du dépot avec description du projet (à quoi ça sert, comment l'installer...)
- `.gitignore` pour ne pas tracer certains fichiers
- `git tag` permet de donner un numéro de release pour garder une version très précise et réutilisable (à faire direct sur github)
- utiliser `CI/CD` pour intégration/déploiement continu => permet de trouver des bugs durant le développement et que le code soit en accord avec standards préalablement paramétrés
- voir liens en diapo 55 pour aller plus loin
- convention pour les messages `git commit`: [conventional commits](https://www.conventionalcommits.org/en/v1.0.0/)

### 7- workflows

Voir [pdf workflows](fairbioinfo-go-2024_workflow_slides.pdf)

3 principaux:
- galaxy
- nextflow
- snakemake
- utilisent tous des images conda, singularity, apptainer, docker

**Galaxy**
- [diapos](https://training.galaxyproject.org/training-material/topics/introduction/tutorials/galaxy-intro-short/slides.html#1)
- facile avec navigateur web
- reproductible (traçable sur galaxy)
- fait pour la bioinfo
- serveur galaxy français
- peut aussi être lancé sur terminal une fois que le workflow est bien défini
- semaine de formation galaxy tous les ans en visio

**Nextflow**
- langage = java
- grosse communauté -> nf-core
- enchainement de modules avec même syntaxe
- choix des profiles: marche mieux avec singularity qu'avec conda

**SnakeMake**
- langage = python
- wrappers dispo sur le site

Pour tous les workflows:
- importance de la communauté => vérifier qu'un outil n'existe pas déjà avant d'en créer un
- workflowHub.eu (support Elixir)
- dockStore.org => + domaine de la santé

Ou les trouver:
- IWC = Intergalactic Workflows Commission => pour valider les workflows => uniquement les bon workflows qui fonctionnent correctement rendus dispos sur galaxy et correctement référencés
- nf-core = curated set of analysis pipelines (pipelines qui fonctionnent ++)
- nf-test = testing framework for nextflow pipeline (récent pour tester pipeline)
- SnakeMake workflow catalog => beaucoup de workflow standardisés

Workflow DAG (pipeline diagram)
- peut-être fait dès le début en SnakeMake
- généré qu'à la fin par nextflow car prend en compte les données réelles données en input

Attention:
- si requete API dans pipeline, mettre des limites de mémoire/temps pour éviter d'être bloqué par l'API trop longtemps
- job array avec même job id est mieux utilisé par les clusters de calcul que plein de job id différents (pour éviter des fils d'attente énormes dans le cluster)