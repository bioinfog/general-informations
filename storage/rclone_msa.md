# Connect from Genouest to msa storage server using rclone

First, ask to be added as authorized user to `msaa4` using [Grouper](https://grouper.univ-rennes1.fr/grouper/).

## Install (and update) rclone

- On genouest, create and activate an `rclone` environment in the `~env/` folder:

```bash
conda create -p ~tderrien/env/rclone rclone
conda activate ~tderrien/env/rclone
```

- Selfupdate :

```bash
rclone selfupdate
2024/05/24 09:50:47 NOTICE: Successfully updated rclone from version v1.65.2-DEV to version v1.66.0
```

- Check help : `rclone --help`

## Configure rclone for msa

Use this command line :
`rclone config`

Then enter these informations:

- New remote = n
- Find a name = storage_dsi
- Type of storage to configure = smb
- SMB server hostname to connect to = partage-igdr.univ-rennes.fr
- user = your username
- port = keep empty
- pass = y ( type in my own password) then your mdp (le mdp est chiffré dans la conf)
- Domain name for NTLM authentication = UR
- spn = keep empty
- Edit advanced config = n


You should see smthg like this:

```bash

Current remotes:

Name                 Type
====                 ====
storage_dsi          smb
```

- Test if ok with `ls`

```bash
rclone ls storage_dsi:rv436-msaa4/ | head
    12292 .DS_Store
     4096 ._.DS_Store
     4096 ._BIOLOGY_DAY_HITTE.pdf
     6148 Genouest_FASTQ_ARCHIVES_2024/.DS_Store
     4096 Genouest_FASTQ_ARCHIVES_2024/._.DS_Store
5829709501 Genouest_FASTQ_ARCHIVES_2024/MelanomaArchive_2024/B00E00E_R1.fastq.gz
5821926788 Genouest_FASTQ_ARCHIVES_2024/MelanomaArchive_2024/B00E00E_R2.fastq.gz
1854289532 Genouest_FASTQ_ARCHIVES_2024/MelanomaArchive_2024/B00E00F_R1.fastq.gz
1865637175 Genouest_FASTQ_ARCHIVES_2024/MelanomaArchive_2024/B00E00F_R2.fastq.gz
5752055884 Genouest_FASTQ_ARCHIVES_2024/MelanomaArchive_2024/B00E00G_R1.fastq.gz
```

## IGDRion = msa3b

- Exemple of directory from p2-solo :
```bash

INPUTDIR="/20241029/baine_X_ADN12918_2_SH_BH/20241029_1611_P2S-00691-A_PAY67577_d27fcad5/pod5"
```

- List files/directory :
`rclone ls storage_dsi:rv436-msab3/$INPUTDIR`


## Copy/Sync data

- [rclone Copy](https://rclone.org/commands/rclone_copy/) : `rclone copy` 
- [rclone sync]( https://rclone.org/commands/rclone_sync/): `rclone sync`

:warning: `rclone sync` make a copy of the local folder  *ie* it removes what does not match on the remote server. Use `--dry-run` option to check first.

:warning:  Always use the option `--bwlimit 10M` not to sature Genouest bandwidth.

### Example1  : Copy data from Genouest to msaa4

```bash
# Create directory on remote server
rclone mkdir storage_dsi:rv436-msaa4/igdrion

# Dry-run to sync nanopore igdrion data from GOLDogs project
rclone sync \
--bwlimit 10M \
--dry-run \
/groups/dog/nanopore/GOLDOGS/ storage_dsi:/rv436-msaa4/igdrion

```

** Script available: [rclone_copy_Genouest2MSA.sh](./rclone_copy_Genouest2MSA.sh).** Tx @AuroreBe

### Example2  : Copy data from msab3 to Genouest (pwd)

Thank @vlebars!
```bash

# Dry-run to sync nanopore igdrion data from GOLDogs project
conda activate /home/genouest/cnrs_umr6290/vlebars/env/rclone

FILE_TO_COPY=storage_dsi:rv436-msab3/20241029/baine_X_ADN12918_2_SH_BH/20241029_1611_P2S-00691-A_PAY67577_d27fcad5/pod5/
OUTDIR=/groups/igdrion/AKC_methylDog/primary/dog/baine_X_ADN12918_2_SH_BH/SQK-LSK114/flowcellprom/raw_data/pod5_pass/

# Note : you can use `rclone copy --dry-run $FILE_TO_COPY $OUTDIR`` to test what it WOULD do WITHOUT actually doing it
# `--progress & --stats 1m` allow to print some info about the copy status in the output, every 1min (1m parameter)
# `--bwlimit 30M` is to set the download speed limit (be careful, it could anger genouest x_x)`

rclone copy --bwlimit 30M --progress --stats 1m $FILE_TO_COPY $OUTDIR

```
