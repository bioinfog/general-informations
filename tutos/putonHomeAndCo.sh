#!/bin/bash

####################################################################
####################################################################
# Thomas DERRIEN , Nov 2022
# tderrien@univ-rennres1.fr
# Create URL from file based on Genouest dog partition through  https://homeandco.genouest.org/
# 
####################################################################

usage() {
    echo "#" >&2
    echo -e "# USAGE: `basename $0` <path/to/file> ">&2
    exit 1;
}
###########################################################@

if [ -f "$1" ]; then
   echo ""
   FILENAME=$(readlink -f $1)
   printf "$FILENAME\t";
else
    echo "#" >&2
    echo "# ERROR: please provide a valid file"
    usage;
    exit 1;
fi

# Credentials to your Genouest/Genossh account
GENOUEST_ID=$USER
# Get password
echo ""
echo -n Password:
read -s password
echo ""
GENOUEST_PWD=$password


# from inside genouest, the certificate is auto-valid so need to put -k
# from elsewhere, remove -k
# Creat token
TOKEN=$(curl --fail --silent -k -u ${GENOUEST_ID}:${GENOUEST_PWD} https://homeandco.genouest.org/api/shares${FILENAME} | grep token | sed -e 's/\"/ /g' | awk '{print $4}')


# Test URL has been created
if [ -z "$TOKEN" ]
then
    echo "#" >&2
    echo "# ERROR: Smthg went wrong... check if file is available on: https://homeandco.genouest.org/"
    usage;
    exit 1;
else
#   printf "$TOKEN\t";
    echo " "
fi

# Make URL with 'download'
URL="https://homeandco.genouest.org/api/download/$FILENAME?token=$TOKEN"

echo "$URL"
