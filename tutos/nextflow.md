# Tutorial for Nextflow usage

## Specify Singularity image directory location

Nextflow use for each process a container. On the Genocluster we can only use singularity or conda. Fortunately, all BioConda image are available in the `/cvmfs/singularity.galaxyproject.org/all/` directory on [Genocluster](https://help.genouest.org/usage/slurm/#singularity-image-catalog). The only issue is that they do not have the correct naming convention for the nf-core pipelines. The links therefore need to be renamed:

```bash
for file in /cvmfs/singularity.galaxyproject.org/all/*; do
    directory="/groups/dog/data/singularity/"
    prefix="depot.galaxyproject.org-singularity-"
    file_modif=$(echo "$(basename $file)" | tr : -)
    suffix=".img"
    newname="$directory$prefix$file_modif$suffix"
    ln -s "$file" "$newname"
done
```

With all those files now renamed you can add in your `~/.bashrc` for Nextflow to use them.
`export NXF_SINGULARITY_CACHEDIR='/groups/dog/data/singularity/'`
`export NXF_APPTAINER_CACHEDIR='/groups/dog/data/singularity/'`

A remaining problem is the latence with the `/cvmfs` partition and it crash from time to time.