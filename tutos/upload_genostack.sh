#!/bin/bash

####################################################################
# Matthias LORTHIOIS, 2022.
# ------------------------------------------------------------------
# Auto-upload TransforKmers model + tokenizer configs (as .zip) to 
# Genostack and auto-generate an URL to download it via HTTP.
#
# How to: Just fill the variables below and run the bash script.
#
####################################################################
# Credentials to your Genouest/Genossh account
GENOUEST_ID="mlorthiois"
GENOUEST_PWD="my_password"

# Informations about project-name (container), models and paths
CONTAINER="transforkmers"
MODEL_PATH="/scratch/mlorthiois/test_bert/models/dog_5prime_bert_6_12-512/best/"
TOKENIZER_PATH="/scratch/mlorthiois/test_bert/config/tokenizer_k6_512"
ZIP_NAME="dog_5prime_bert_6-512"

# Key (a random complex string) to authenticate URL link
SWIFT_KEY="b3968d0207b54ece87cdce06515a89d4"

####################################################################
. /local/env/envconda3.sh
conda activate ~/workspace/conda/bio_tools/ # Or any conda env with python-swiftclient and python-keystoneclient installed

export PREFIX_URL="https://genostack-api-swift.genouest.org"
export OS_AUTH_URL="https://genostack-api-keystone.genouest.org/v3"
export OS_PROJECT_NAME=$GENOUEST_ID
export OS_USERNAME=$GENOUEST_ID
export OS_PASSWORD=$GENOUEST_PWD
export OS_PROJECT_DOMAIN_NAME="Users"
export OS_USER_DOMAIN_NAME="Users"
export OS_IDENTITY_API_VERSION=3

####################################################################
# CREATE ZIP
zip -j -9 ${ZIP_NAME}.zip $MODEL_PATH/* $TOKENIZER_PATH/* > /dev/null

# UPLOAD ZIP TO GENOSTACK
swift upload ${CONTAINER} ${ZIP_NAME}.zip > /dev/null

# RETRIEVE AUTH_ID
AUTH_ID=$(swift auth | awk -F":8080" 'NR==1 { print $2}')

# GET URL_KEY FOR TEMPURL
swift post -m "Temp-URL-Key:${SWIFT_KEY}"

# CREATE TEMPURL
URL=$(swift tempurl GET 1000000000 "${AUTH_ID}/${CONTAINER}/${ZIP_NAME}.zip" "${SWIFT_KEY}")

# WRITE FINAL URL
echo "${PREFIX_URL}${URL}"
