# VM IGDR

Virtual machines are provided by the DSI.
You need to create a ticket in `DSI-INFRA+ASN`.
A service contract is established with *Sudoer* access.

## Accès

You can access the VM via SSH.

```bash
ssh :USER_NAME:@:VM_ID:.univ-rennes.fr
```

Or access via [PROMOX](https://pveceph-p01.univ-rennes1.fr:8006/)
using the ENT identifiers.

This allows you to view the VM (search bar), resources, logs, snapshots, backups, tasks, settings, permissions.
A console access is possible to connect to the VM, turn it off, turn it back on, ...

To connect, it is necessary to have selected `/usr/bin/bash` in [Sesame](https://sesame.univ-rennes1.fr/comptes/)
and not `/usr/local/bin/ur1noshell`.

## Shiny application

### Conda install

To install conda, follow the instructions on the [official website](https://docs.anaconda.com/anaconda/install/linux/).

```bash
yum install libXcomposite libXcursor libXi libXtst libXrandr alsa-lib mesa-libEGL libXdamage mesa-libGL libXScrnSaver
curl -O https://repo.anaconda.com/archive/Anaconda3-2024.06-1-Linux-x86_64.sh
shasum -a 256 Anaconda3-2024.06-1-Linux-x86_64.sh
bash Anaconda3-2024.06-1-Linux-x86_64.sh

source <PATH_TO_CONDA>/bin/activate
conda init
```

### Shiny environment

You then need to create the environment your shiny app will run in.

```bash
conda env create --name env_shiny --file environment.yml
conda activate env_shiny
```

And to install your application inside the environment.

```R
if (!require("remotes", quietly = TRUE))
    install.packages("remotes")

remotes::install_github(":USER_NAME:/:REPOSITORY:", ref = "devel", dependencies = TRUE, upgrade = "always",
    build_vignettes=TRUE
)
```

### R shiny application

You will need a directory with a `app.R` file in it where your shiny application will be called.
This should be the same as the one mentionned below:

`/private/staff/t/et/:USER_NAME:/:DIR_SHINY_APP:`

Ensure that the `app.R` shiny app use the port **3838** or the one given by the *DSI*.

## Shiny server

You can follow the instructions [shiny server from source](https://github.com/rstudio/shiny-server/wiki/Building-Shiny-Server-from-Source) or
[shiny server from compile](https://posit.co/download/shiny-server/).

```bash
sudo yum install R
sudo su - -c "R -e \"install.packages('shiny', repos='http://cran.rstudio.com/')\""
wget https://download3.rstudio.org/centos7/x86_64/shiny-server-1.5.22.1017-x86_64.rpm
shasum -a 256 shiny-server-1.5.22.1017-x86_64.rpm

sudo yum install --nogpgcheck shiny-server-1.5.22.1017-x86_64.rpm
# Change `run_as shiny` to `run_as :HOME_USER: shiny;`
sudo nano /etc/shiny-server/shiny-server.conf
sudo systemctl restart shiny-server
cat ~/log-shiny
systemctl --type=service | grep shiny
```

The `shiny-server.conf` file should look like this:

```conf
# Define the user we should use when spawning R Shiny processes
run_as :USER_NAME: shiny;

# Define a top-level server which will listen on a port
server {
  # Instruct this server to listen on port 3838
  listen 3838 127.0.0.1;

  # Define the location available at the base URL
  location / {
    # Run this location in 'site_dir' mode, which hosts the entire directory
    # tree at '/srv/shiny-server'
    site_dir /private/staff/t/et/:USER_NAME:/:DIR_SHINY_APP:;
    
    # Define where we should put the log files for this location
    log_dir /private/staff/t/et/:USER_NAME:/log_shiny;
    
    # Should we list the contents of a (non-Shiny-App) directory when the user 
    # visits the corresponding URL?
    directory_index on;
  }
}

```

### Use conda environment

For the shiny server to use the conda environment you created

```bash
cat /etc/systemd/system/shiny-server.service
sudo nano /etc/systemd/system/shiny-server.service
```

and add the following

```conf
[Service]
Environment="R=/private/staff/t/et/:USER_NAME:/anaconda3/envs/env_shiny/bin/R"
```

## Use HTTPS proxy

Need to follow the [tutorial](https://ipub.com/shiny-https/) or this [tutorial](https://support.posit.co/hc/en-us/articles/213733868-Running-Shiny-Server-with-a-Proxy)

### Install Nginx for Proxy

Follow tutorial with [NGINX](https://www.digitalocean.com/community/tutorials/how-to-set-up-shiny-server-on-ubuntu-20-04)

```bash
sudo yum install nginx
sudo yum install ufw
sudo ufw allow 80,443/tcp

sudo ss -plut | grep shiny # Should listen on 3838
```

### Install SSL certificate

Ask DSI for frontalisation
Install SSL certificate on the VM

```bash
sudo mkdir -p /etc/ssl/private/
sudo mv frontalufr-017.univ-rennes.fr.crt /etc/ssl/
sudo mv frontalufr-017.univ-rennes.fr.key /etc/ssl/private/
```

### Add to Nginx conf ssl proxy

```bash
cat /etc/shiny-server/shiny-server.conf
cat /etc/nginx/nginx.conf
sudo nano /etc/nginx/nginx.conf
```

Add the following

```conf
http {
    ...
    # Map proxy settings for RStudio
    map $http_upgrade $connection_upgrade {
        default upgrade;
        '' close;
    }
    ...
    server {
        listen              443 ssl;
        server_name         www.:SSL_NAME:.univ-rennes.fr;
        ssl_certificate     /etc/ssl/frontalufr-017.univ-rennes.fr.crt;
        ssl_certificate_key /etc/ssl/private/frontalufr-017.univ-rennes.fr.key;
        location / {
            proxy_pass http://127.0.0.1:3838;
            proxy_redirect http://127.0.0.1:3838/ https://$host/;
            proxy_http_version 1.1;
            proxy_set_header Upgrade $http_upgrade;
            proxy_set_header Connection $connection_upgrade;
            proxy_read_timeout 20d;
        }
        error_page 404 /404.html;
        location = /40x.html {
        }
        error_page 500 502 503 504 /50x.html;
        location = /50x.html {
        }
    }
}
```

### Restart service

```bash
# Check config
sudo nginx -t
#sudo unlink /etc/nginx/sites-enabled/default

sudo systemctl stop shiny-server
sudo systemctl daemon-reload 
sudo systemctl start shiny-server
sudo systemctl restart nginx
```

## Add automatic start

You need to add conda environment activation to `~/.bashrc` file.

```bash
echo "conda activate env_pedixplorer" >> ~/.bashrc
```

But you also need all the services to be restart when the VM reboot.
To do so you need to create a new service in `/etc/systemd/system`.

```bash
sudo nano /etc/systemd/system/reboot.service
```

and add in it

```service
[Unit]
Description=Reboot VM and launch services

[Service]
Type=simple
ExecStart=/bin/bash /private/staff/t/et/:USER_NAME:/setup_vm.sh

[Install]
WantedBy=multi-user.target
```

You then need to create `setup_vm.sh` file containing the following commands.

```bash
#!/bin/bash

sudo systemctl stop shiny-server
sudo systemctl daemon-reload 
sudo systemctl start shiny-server
sudo systemctl restart nginx
```

As the command in the `setup_vm.sh` file requires sudo rights, you need to add the following line to the `/etc/sudoers` file.

```bash
:USER_NAME: ALL=(ALL) NOPASSWD: /bin/systemctl stop shiny-server
:USER_NAME: ALL=(ALL) NOPASSWD: /bin/systemctl daemon-reload
:USER_NAME: ALL=(ALL) NOPASSWD: /bin/systemctl start shiny-server
:USER_NAME: ALL=(ALL) NOPASSWD: /bin/systemctl restart nginx

root ALL=(ALL) NOPASSWD: /bin/systemctl stop shiny-server
root ALL=(ALL) NOPASSWD: /bin/systemctl daemon-reload
root ALL=(ALL) NOPASSWD: /bin/systemctl start shiny-server
root ALL=(ALL) NOPASSWD: /bin/systemctl restart nginx
```
