#!/bin/sh

# Date : 28/03/2024
# Authour
# Contact
##############################################

# input parameters
INFILE=$1
PROGNAME=$0

# Usage of the prog
usage (){

	echo "# Error..." >&2 
	echo "# Format a Nanoready .csv file into Modul-Bio ..." >&2 
	echo "# Usage :`basename $0` IN.CSV"
}


# Test input file readable
if [ ! -r "$INFILE" ];then

	echo "ERROR: Input file not readable"
	usage;
	
else

	echo""
	echo "	Mise en forme du fichier $INFILE"
	echo""
	
cat $INFILE | \

sed -e 's/;/ /g' | \

awk 'NR>2{print $1,$2}' | \

sed -e 's/,/ /g' | \

awk 'BEGIN{print "#;Sample_ID;User_name;Date;Nucleic_Acid_Conc;Unit;A260;A280;260/280;260/230;Sample_type;Factor"}{OFS=";"; print $1,$3,"rguyon",$5,$7,"ng/uL",$10,$11,$8,$9,"DNA","50"}' | \

sed -e 's/\./,/g' > MBio_$1

	echo "	Le fichier MBio_$INFILE est prêt pour importation dans Modul-Bio"
	echo""

fi
