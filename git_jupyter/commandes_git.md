# Récupérer les données distantes / initialiser un nouveau répertoire

Pour initialiser un nouveau projet (un projet qui n'existe pas encore sur git), on utilise:
```bash
git init
```

S'il existe déjà sur github / gitlab, on le clone via la commande :
```bash
git clone {lien_du_repo}
```

Une fois cloné, et si des modifications sont apportées sur gitlab (par quelqu'un d'autre par exemple), on actualise sa version locale via la commande:
```bash
git fetch
git pull origin master
```

# Faire un enregistrement/commit

L'objectif du commit est de faire une capture/photo du projet actuel. Pour cela, 3 étapes : 
1. Ajouter les fichiers à la "photo"
2. Lister les ajouts/modifications par rapport à l'ancien commit
3. Push sur gitlab/github.

### Ajouter les fichiers modifiés au commit

On sélectionne les fichiers que l'on souhaite enregistrer :

```bash
git add nom_fichier
# Ou :
git add . #pour ajouter tous les fichiers modifiés d'un coup
```

### Enregistrement

On fait un commit avec un message expliquant les modifications par rapport à l'ancien commit :

```bash
git commit -m "Message"
```

### Push son répertoire sur le dépôt distant GitHub

```bash
git push origin master
```

# Branches

**Optionnel** : On peut créer une branche sur laquelle on peut faire des modifications/ajouts de fonctionnalités. On pourra travailler dessus. Si les modifications nous plaisent, on peut les rappatrier dans le projet initial, sinon, on peut la supprimer.

### Création d'une banche

```bash
git branch nom_de_la_branche
```

**Attention après création, ne pas oublir de changer de branche pour faire les modifications.**

### Changer de branche

```bash
git checkout nom_de_la_branche
```

### Fusionner le master et la branche

Ceci se fait en 3 étapes :

1. Aller sur la branche qui va **recevoir** les modifications (master) via `git checkout master`
2. Ensuite, on demande de rapatrier le travail de la branche sur le master via `git merge nom_de_la_branche`
3. Enfin, on peut supprimer la branche rapatriée via `git branch -d nom_de_la_branche`

### Listing des branches

```bash
git branch
```

# Autres commandes utiles

### Retour à l'ancien commit

Si, sans avoir créer de branche, on veut retourner au dernier commit/ à un ancien commit, on fait pour retourner au dernier commit:
```bash
git checkout
```

ou on trouve l'ID du commit auquel on veut retourner avec `git log` puis :
```bash
git revert --no-commit {id}..HEAD
git commit
````
**Attention, en faisant cela, toutes les modifications faites entre l'ID choisi et l'état actuel du répertoire est perdu.**

### Timeline des commits

Permet de donner l'historique des modifications par ordre chronologique.

```bash
git log
```

### Voir l'état de son répertoire

On peut voir les modifications apportées en local par rapport au dernier commit via la commande :
```bash
git status
```

### Créer un fichier .gitignore

Si on veut que certaines fichiers/dossiers ne soient pas pris en compte par git (notamment les fichiers log, les dossiers résultats, etc), on créer un fichier `.gitignore` à la racine du répertoire, et on rentre à l'intérieur les fichiers/dossiers qu'on veut ignorer.

Exemple :

```log
results/
slurm*
.conda
```